import yaml

conf_file = "file_sync_conf.yaml"


def read_file(conf):
    with open(conf) as file:
        conf_yaml = yaml.load(file, Loader=yaml.FullLoader)

    for number, _ in enumerate(conf_yaml["start"]):
        print("<<<<<<<<<< config: ", number, ">>>>>>>>>>")
        for count, i in enumerate(conf_yaml["start"][number]["new"]):
            print(i)
            print("count:", count)
            if conf_yaml["start"][number]["new"][count]["path"]["ssh"]["enabled"]:
                if conf_yaml["start"][number]["new"][count]["path"]["ssh"]["ssh_key"]["enabled"]:
                    print("---> ssh_key:",
                          conf_yaml["start"][number]["new"][count]["path"]["ssh"]["ssh_key"]["enabled"])
                    print("user:", conf_yaml["start"][number]["new"][count]["path"]["ssh"]["ssh_key"]["user"])
                    print("password:", conf_yaml["start"][number]["new"][count]["path"]["ssh"]["ssh_key"]["key"])
                    print("dir:", conf_yaml["start"][number]["new"][count]["path"]["ssh"]["ssh_key"]["dir"])
                    print("server:", conf_yaml["start"][number]["new"][count]["path"]["ssh"]["ssh_key"]["server"])
                    print("port:", conf_yaml["start"][number]["new"][count]["path"]["ssh"]["ssh_key"]["port"])
            if conf_yaml["start"][number]["new"][count]["path"]["ssh"]["enabled"]:
                if conf_yaml["start"][number]["new"][count]["path"]["ssh"]["u_passwd"]["enabled"]:
                    print("---> u_passwd:",
                          conf_yaml["start"][number]["new"][count]["path"]["ssh"]["u_passwd"]["enabled"])
                    print("user:", conf_yaml["start"][number]["new"][count]["path"]["ssh"]["u_passwd"]["user"])
                    print("password:", conf_yaml["start"][number]["new"][count]["path"]["ssh"]["u_passwd"]["passwd"])
                    print("dir:", conf_yaml["start"][number]["new"][count]["path"]["ssh"]["u_passwd"]["dir"])
                    print("server:", conf_yaml["start"][number]["new"][count]["path"]["ssh"]["u_passwd"]["server"])
                    print("port:", conf_yaml["start"][number]["new"][count]["path"]["ssh"]["u_passwd"]["port"])
            if conf_yaml["start"][number]["new"][count]["path"]["local"]["enabled"]:
                print("---> local:", conf_yaml["start"][number]["new"][count]["path"]["local"]["enabled"])
                print("dir:", conf_yaml["start"][number]["new"][count]["path"]["local"]["dir"])
            if conf_yaml["start"][number]["new"][count]["path"]["replace_space"]["enabled"]:
                print("---> replace_space:",
                      conf_yaml["start"][number]["new"][count]["path"]["replace_space"]["enabled"])
            if conf_yaml["start"][number]["new"][count]["path"]["syn_dir"]["enabled"]:
                print("---> syn_dir:", conf_yaml["start"][number]["new"][count]["path"]["syn_dir"]["enabled"])
                print("put_var:", conf_yaml["start"][number]["new"][count]["path"]["syn_dir"]["put_var"])
            if conf_yaml["start"][number]["new"][count]["path"]["syn_files"]["enabled"]:
                print("---> syn_files:", conf_yaml["start"][number]["new"][count]["path"]["syn_files"]["enabled"])
                print("put_var:", conf_yaml["start"][number]["new"][count]["path"]["syn_files"]["put_var"])
            if conf_yaml["start"][number]["new"][count]["path"]["not_see_dirs"]["enabled"]:
                print("---> not_see_dirs:", conf_yaml["start"][number]["new"][count]["path"]["not_see_dirs"]["enabled"])
                print("dirs:", conf_yaml["start"][number]["new"][count]["path"]["not_see_dirs"]["dirs"])
            if conf_yaml["start"][number]["new"][count]["path"]["not_see_files"]["enabled"]:
                print("---> not_see_files:",
                      conf_yaml["start"][number]["new"][count]["path"]["not_see_files"]["enabled"])
                print("files:", conf_yaml["start"][number]["new"][count]["path"]["not_see_files"]["files"])
                print("")


def main():
    read_file(conf_file)


if __name__ == '__main__':
    main()
